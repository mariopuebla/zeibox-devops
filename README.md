# Zeibox-Devops

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.0.

# 0

```
ng new zeibox-devops
```


# 1

```
customLaunchers: {
  ChromeHeadlessCI: {
    base: 'ChromeHeadless',
    flags: ['--no-sandbox']
  }
},
```


# 2

```
"test:ci": "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI",
```

# 3

```
image: node:10.16.0

stages:
  - install
  - test
  - build
  - deploy
```

# 4

```
install:
  stage: install
  script: 
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/
```

# 5

```
tests:
  stage: test
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install
  before_script:
    - apt-get update && apt-get install -y apt-transport-https
    - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    - sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    - apt-get update && apt-get install -y google-chrome-stable
  script:
    - npm run test:ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'
```

# 6

```
build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
```

# 7

```
"build:ci": "ng build -c=$BUILD_CONFIG --baseHref=/zeibox-devops/",
```

# 8

```
build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
  only:
    - master
```


# 9

```
pages:
  stage: deploy
  dependencies:
    - build
  script:
    - mkdir public
    - mv ./dist/zeibox-devops/* ./public/
    - cp ./public/index.html public/404.html
  artifacts:
    paths:
      - public/
  environment:
    name: production
  only:
    - master
```