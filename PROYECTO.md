# Proyecto Angular para CI / CD

### Crear el proyecto en versiones de Angular anteriores a la 9 

```
ng new zeibox-devops --enable-ivy
```


### Crear el proyecto en Angular 9 

```
ng new zeibox-devops
```


### Definición del Ciclo

1. Dependencies (Instalar dependencias)
2. Testing (Correr pruebas)
3. Build (Si todo sale bien, construir la imagen de la aplicación)
4. Deploy (Desplegar la aplicación)


## Que es CI (Continuous Integration)

#### En Angular CI implica el Testing (pasos 1 y 2)

Vitales para asegurar la calidad del software. Se compone de:

* Pruebas unitarias
* Pruebas de integración

Estos puntos deben ser implementados en porcesos de automatización en un servidor, para poder hacer deploy a producción de forma automática.

#### Requsitos para implementar CI

* Se debe establecer la forma en que Angular correrá las pruebas y tendremos que asegurarnos que corra bien en un servidor de integración continua.

NOTA: 
* Cuando corremos pruebas en un ámbito local, dependemos de un navegador (normalmente Chrome). Un servidor de CI no tiene Chrome, por ello deberemos configurarlo, ya que no tendrá una interfaz gráfica

* Para esto entraremos en el archivo karma.config.js y debajo de lo siguiente:
```
browsers: ['Chrome'],
```
Insertaremos las siguientes líneas
```
customLaunchers: {
  ChromeHeadlessCI: {
    base: 'ChromeHeadless',
    flags: ['--no-sandbox']
  }
},
```

#### Luego generaremos una nueva forma de correr pruebas de Angular

```
ng test
--no-watch
--code-coverage
--browsers=ChromeHeadlessCI
```
* --no-watch, le dice al servidor que se ejecute sólo una vez
* Luego generaremos el --code-coverage o Reporte de Cobertura
* Por último le indicamos con --browsers=ChromeHeadlessCI, que el browser será ChromeHeadlessCI

#### Crear un proyecto en HitLab como público
Para obtener la mayoría de las herramientas que ofrece la plataforma

#### Instrucciones en línea de comando
También puede cargar archivos existentes desde su computadora utilizando las instrucciones a continuación.

Configuración global de Git
```
git config --global user.name "Mario Alberto Puebla"
git config --global user.email "mariopuebla@gmail.com"
```
Crear un nuevo repositorio
```
git clone https://gitlab.com/mariopuebla/zeibox-devops.git
cd zeibox-devops
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
Hacer Push a una carpeta existente
```
cd existing_folder
git init
git remote add origin https://gitlab.com/mariopuebla/zeibox-devops.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
Hacer Push a un repositorio de Git existente
```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/mariopuebla/zeibox-devops.git
git push -u origin --all
git push -u origin --tags
```
#### Crear un Issues
* Title: Crear stages de Install y Test
* Assign to me (La asigno a mi)
* Description (write): /estimate 2h
* Submit issues
* Puedo ir a Board y ver el Issue creado
* Add default list
* Muevo (como en Tello) el issue a Doing
* Hacer click en Crear stages de Install y Test
* Crear una rama \/ Selecciona: Create merge request and branch
* Copiar el nombre de la rama (copy branch name)

Fingerprints
id_rsa.pub
```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKJJFSC+t81VCUzUvdol6iKDzdV7OtMa6IfLgIo4FyY5Ib3AZ+UvECp1j4Wsli6AEY6Of8s+pGcHODFthVzr8qKbo8ClMHna0SsIujJbRtb3BMV6Act5C5hGq7al7uGnfOUc2WvLB4FXGRitA7MWKn5SAPjj4dP+VsjUS5fNCukcE9dIHpuXgnmqYnmEPZTxKqMDkHSUeM8x9w1m391nGzuTRfCyAnUfg9Gg8h966yPV9fc/4JqOaWRWmrmeC49I7Hqq1FpvbNnQto911QEW344wjEpI6mo/QhOCJwxHarhm/0qJVlNYVmfW55SpzVq7tpU6z1bOoQR553e3i89K6r
```
```
MD5: fa:e5:84:65:d7:82:74:39:2b:dd:8c:43:e0:d7:58:29
SHA256: PouW83AGFJLuT7VtCFYW6hKsDUF3KSXZb8tDjpiVzpM
```

#### Agregar una línea en package.json
```
"test:ci": "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI",
```
Vamos a la línea de comandos y ejecutamos
```
npm run test:ci
```
